#*****************************************************************#
#* *************   auto-py-to-exe et convertico   ************** *#
#*****************************************************************#
""" 
    - pip install auto-py-to-exe   >>> pour installer le fichier de transformation en executable
    - https://convertico.com/      >>> pour convertir en icon
"""

#*****************************************************************#
#* ***************   Importation des modules      ************** *#
#*****************************************************************#
import tkinter
from tkinter import messagebox

#*****************************************************************#
#* ***************   Parametres de fenetres       ************** *#
#*****************************************************************#
def screenCenter(app, title):
    app['bg'] = "#C0C0C0"
    app.title(title)
    app.resizable(width = False, height = False)
    screenX = int(app.winfo_screenwidth()) 
    screenY = int(app.winfo_screenheight())
    windowsX = screenX // 2
    windowsY = screenY // 2
    posX, posY = (screenX // 2) - (windowsX // 2), (screenY // 2) - (windowsY // 2)
    geometry = "{}x{}+{}+{}".format(windowsX, windowsY, posX, posY)
    app.geometry(geometry)
    return windowsX, windowsY

#*****************************************************************#
#* ***************       Gestion d'erreur         ************** *#
#*****************************************************************#
def showWindows(args):
    if args == "error":
        messagebox.showerror("ERROR", "Une erreur a été detecté !")
    elif args == "alert":
        messagebox.showwarning("ATTENTION", "Les données ne sont pas renseigné !")
    elif args == "info":
        messagebox.showinfo("INFOS", "Données incorrectes !")

def test(varMass, varSize):
    try:
        varMass, varSize = float(varMass), float(varSize)
        assert 1 <= varMass <= 300 and 0.1 <= varSize <= 3
        result = imcCalculator(varMass, varSize)
        displayResult(frameResult, result, varCkeckSex.get())
    except ValueError:
        return showWindows(args = "error")
    except AssertionError:
        return showWindows(args = "info")

#*****************************************************************#
#* ***************          ImcCalculator         ************** *#
#*****************************************************************#
def imcCalculator(mass, size):
    mass, size = float(mass), float(size)
    imc = mass / (size**2)
    if imc < 18.5:
        result = ["{} : (Vous êtes en Insuffisance\n ponderale)".format(round(imc, 3)), "Vous êtes en situation de maigreur. Pas de quoi s'inquiéter. Consommer plus régulièrement des repas équilibrés et complets.(voir un diététicien pour plus de conseils)"]
    elif 18.5 <= imc < 24.9:
        result = ["{} : (Vous êtes Normal)".format(round(imc, 3)), "Vous avez une corpulence normale, pour maintenir votre forme, manger cinq fruits et légumes par jour et pratiquez une activité physique régulière"]
    elif 24.9 <= imc < 30:
        result = ["{} : (Vous êtes en Surpoids)".format(round(imc, 3)), "IMC élevé indique que vous êtes en surpoids. Manger moins gras et plus de fruits et légumes, consultez un diététicien pour vous aidez dans vos choix alimentaires"]
    else:
        result = ["{} : (Vous êtes Obese)".format(round(imc, 3)), "A ce stade, vous souffrez d'obesité. Vous êtes considéré comme une personne exposée à un risque d'infractus, diabète, AVC. Il est important de pratiquer une activité physique tout en optant pour une alimentation équilibrée"]
    return result

#*****************************************************************#
#* ***************      Gestion d'affichage       ************** *#
#*****************************************************************#
def getDataMassSize(*args):
    pass

def displayResult(frameResult, result, sexs):
    frameResult.destroy()
    widthResult = sizeScreen[0] - widthApp
    heightResult = (sizeScreen[1] * 4) // 5
    frameResult = tkinter.LabelFrame(app, text = "RESULTATS", width = widthResult, height = heightResult, borderwidth = 5, font = "algerian 10")
    frameResult.place(x = widthApp, y = 0)
    frameResult['bg'] = "#FFFFBF"

    if int(sexs) == 0:
        sex = "Monsieur"
    else:
        sex = "Madame"
    varWelcom = tkinter.IntVar()
    welcom = tkinter.Label(frameResult, textvariable = varWelcom, font = "Bahnschrift 15")
    varWelcom.set("Bienvenu {}, sur IMCAPPLIS".format(sex))
    welcom.place(x = 20, y = 5)
    welcom['bg'] = "#FFFFBF"

    varResultImc = tkinter.StringVar()
    resultImc = tkinter.Label(frameResult, textvariable = varResultImc, font = "Bahnschrift 15")
    varResultImc.set("IMC = {}".format(result[0]))
    resultImc.place(x = 5, y = 40)
    resultImc['bg'] = "#FFFFBF"

    varResultAdvice = tkinter.StringVar()
    resultAdvice = tkinter.Message(frameResult, textvariable = varResultAdvice, font = "Bahnschrift 15")
    varResultAdvice.set("CONSEILS :\n\n>>> {}".format(result[1]))
    resultAdvice.place(x = 5, y = 100)
    resultAdvice['bg'] = "#FFFFBF"

#*****************************************************************#
#* ***************     Parametres des boutons     ************** *#
#*****************************************************************#
def getData(*args):
    if varMassEntry.get() == "" or varSizeEntry.get() == "":
        return showWindows(args = "alert")
    else:
        test(varMassEntry.get(), varSizeEntry.get())


#*****************************************************************#
#* ***************               Applis           ************** *#
#*****************************************************************#
app = tkinter.Tk()
sizeScreen = screenCenter(app = app, title = "IMC")

#* ***************                Menu            ************** *#
appMenu = tkinter.Menu()
firstMenu = tkinter.Menu(appMenu, tearoff = 0)
firstMenu.add_command(label = "Assistance sportif", font = "Constantia 12")
firstMenu.add_command(label = "Assistance diététique", font = "Constantia 12")
secondMenu = tkinter.Menu(appMenu, tearoff = 0)
secondMenu.add_command(label = "quitter", font = "Constantia 12", command = app.quit)
appMenu.add_cascade(label = "Assistances", menu = firstMenu)
appMenu.add_cascade(label = "Fermer", menu = secondMenu)
app.config(menu = appMenu)

#* ***************              FrameApp          ************** *#
widthApp = (sizeScreen[0]) // 2
heightApp = sizeScreen[1]
frameApp = tkinter.LabelFrame(app, text = "APPLICATION", width = widthApp, height = heightApp, borderwidth = 5, font = "algerian 10")
frameApp.place(x = 0, y = 0)
frameApp['bg'] = "#A577F7"

#* ***************              CkeckSex          ************** *#
varCkeckSex = tkinter.IntVar()
varCkeckSex.trace("w", getDataMassSize)
ckeckSexH = tkinter.Radiobutton(frameApp, text = "Homme", font = "Constantia 18", value = 0, variable = varCkeckSex)
ckeckSexH['bg'] = "#A577F7"
ckeckSexH.place(x = 50, y = 30)
ckeckSexF = tkinter.Radiobutton(frameApp, text = "Femme", font = "Constantia 18", value = 1, variable = varCkeckSex)
ckeckSexF.place(x = 180, y = 30)
ckeckSexF['bg'] = "#A577F7"

#* ***************             MassLabel          ************** *#
massLabel = tkinter.Label(frameApp, text = "Masse (kg)    :", font = "Constantia 18")
massLabel.place(x = 50, y = 120)
massLabel['bg'] = "#A577F7"
varMassEntry = tkinter.StringVar()
varMassEntry.trace("r", getDataMassSize)
massEntry = tkinter.Entry(frameApp, width = 5, textvariable = varMassEntry, font = "Bahnschrift 20")
massEntry['bg'] = "#FFFFBF"
massEntry.place(x = 250, y = 120)

#* ***************             SizeLabel          ************** *#
sizeLabel = tkinter.Label(frameApp, text = "Taille (m)      :", font = "Constantia 18")
sizeLabel.place(x = 50, y = 230)
sizeLabel['bg'] = "#A577F7"
varSizeEntry = tkinter.StringVar()
varSizeEntry.trace("r", getDataMassSize)
sizeEntry = tkinter.Entry(frameApp, width = 5, textvariable = varSizeEntry, font = "Bahnschrift 20")
sizeEntry['bg'] = "#FFFFBF"
sizeEntry.place(x = 250, y = 230)

#* ***************            BtnValidete         ************** *#
btn = tkinter.Button(frameApp, text = "VALIDER", font = "algerian 20", borderwidth = 5, width = 8, height = 1, command = getData)
btn.place(x = 120, y = 330)
btn['bg'] = "#9EFD38"

#* ***************           FrameResult          ************** *#
widthResult = sizeScreen[0] - widthApp
heightResult = (sizeScreen[1] * 4) // 5
frameResult = tkinter.LabelFrame(app, text = "RESULTATS", width = widthResult, height = heightResult, borderwidth = 5, font = "algerian 10")
frameResult.place(x = widthApp, y = 0)
frameResult['bg'] = "#FFFFBF"

#* ***************              BtnQuit           ************** *#
btnQuit = tkinter.Button(app, text = "QUITTER", font = "algerian 20", width = 8, height = 1, command = app.quit, borderwidth = 5)
btnQuit.place(x = widthApp + 120, y = heightResult + 15)
btnQuit['bg'] = "#FFA500"

app.mainloop()